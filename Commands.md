# Comandos útiles

## Contenido
- 📚 Operaciones de bases
- 📕 Operaciones con tablas.
- 📄 Operaciones con registros.
- 🗳 Importaciones.

---
</br>
<h2 style='text-align: center' >
📚 Operaciones de bases
</h2>
</br>

> **Listar bases** en el server
```sql
show databases;
```
> **Crear una base** de datos
```sql
CREATE DATABASE <databaseName>;
CREATE DATABASE IF NOT EXISTS <databaseName>; -- Si no existe la base, la creará de otra forma no arrojara un error.
```

> **Desplegar** la **base** **seleccionada**
```sql
select database();
```

> **Cambiar** de **base** de datos
```sql
use <databaseName>;
```

---

</br>
<h2 style='text-align: center' >
📕 Operaciones con tablas.
</h2>
</br>

*Puede más detalles de algunas operaciones en el archivo [1-create.sql](books/1-create.sql)*


> **Listar** las **tablas**
```sql
show tables;
```

> **Describir** una **tabla**.
```sql
describe <table>;
desc <table>; -- Desc es lo mismo que describe
show full columns from <table>; -- Muestra el detalle completo de las columnas
```

> **Crear** una **tabla**  
```sql
$ CREATE TABLE IF NOT EXISTS <table> (
  ...
)
```

> **Renombrar** una **tabla**
```sql
RENAME TABLE <actualName> TO <newName>;
```

> **Eliminar** una **tabla**
```sql
drop table <table>;
```

> **Renombrar** una **columna**
```sql
ALTER TABLE <table>
RENAME COLUMN <acutalName> to <newName>;
```
---

</br>
<h2 style='text-align: center' >
📄 Operaciones con registros.
</h2>
</br>

*Puede más detalles de algunas operaciones en el archivo [2-insert.sql](books/2-insert.sql)*


> **Insertar** un **registro**
```sql
INSERT INTO <table> (...columns) VALUES(...values);
```

*Puede ver más detalles con los select en el archivo [3-select.sql](books/3-select.sql)*

> **Seleccionar** un registro

```sql
SELECT ... FROM ... WHERE;
```



*Puede ver más detalles con los joins en el archivo [4-join.sql](books/4-join.sql)*

> Inner Join 
```sql
SELECT a.author_id, a.name, b.title, b.year FROM authors as a JOIN books as b ON a.author_id = b.author_id; 
```
> Left join

```sql
SELECT a.author_id, a.name, b.title, b.year FROM authors as a LEFT JOIN books as b ON a.author_id =
b.author_id;
```

---

</br>
<h2 style='text-align: center' >
🗳 Importaciones.
</h2>
</br>

> Importar con ayuda de unix
```bash
$ mysql -u root -p < file.sql # Pasa el contenido del archivo .sql a prompt del mysql
$ mysql -u root -D database -p < file.sql # Indicamos explícitamente el nombre de la base.
```


