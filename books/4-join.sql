-- Join o INNER Join, puede entenderse como intersección de dos tablas.
-- Se define una tabla pivote (books) y la tabla o tablas de las cuales se obtendrán nuevos datos (authors)

SELECT b.book_id, a.name, b.title
FROM books as b
JOIN authors as a
  ON a.author_id = b.author_id;

-- El Left Join, pensando en conjuntos, traerá la información basada en la tabla pivote sin importar si no existen datos en las tablas adicionales.

-- INNER JOIN
SELECT a.author_id, a.name, b.title, b.year FROM authors as a JOIN books as b ON a.author_id = b.author_id;

-- LEFT JOIN 
SELECT a.author_id, a.name, b.title, b.year FROM authors as a LEFT JOIN books as b ON a.author_id =
b.author_id;
