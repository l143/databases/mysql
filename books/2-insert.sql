-- ░█▀▀█ █░░█ ▀▀█▀▀ █░░█ █▀▀█ █▀▀█ █▀▀ 
-- ▒█▄▄█ █░░█ ░░█░░ █▀▀█ █░░█ █▄▄▀ ▀▀█ 
-- ▒█░▒█ ░▀▀▀ ░░▀░░ ▀░░▀ ▀▀▀▀ ▀░▀▀ ▀▀▀

INSERT INTO authors(name, nationality)
VALUES('Juan Rulfo', 'MEX');

INSERT INTO authors(name, nationality)
VALUES('Gabriel García Márquez', 'COL');

-- Nosotros definimos el id
INSERT INTO authors
VALUES(3, 'Juan Gabriel Vazquez', 'COL');

-- Podemos ingresar varios registros con un solo query

INSERT INTO authors(name, nationality)
VALUES('Julio Cortazar', 'ARG'),
  ('Isabel Allende', 'CHI'),
  ('Octavio Paz', 'MEX');



-- ▒█▀▀█ █░░ ░▀░ █▀▀ █▀▀▄ ▀▀█▀▀ █▀▀ 
-- ▒█░░░ █░░ ▀█▀ █▀▀ █░░█ ░░█░░ ▀▀█ 
-- ▒█▄▄█ ▀▀▀ ▀▀▀ ▀▀▀ ▀░░▀ ░░▀░░ ▀▀▀

INSERT INTO clients (name, email, birthdate, gender, active)
VALUES ('Maria Dolores Gomez','Maria Dolores.95983222J@random.names','1971-06-06','F', 1),
('Adrian Fernandez','Adrian.55818851J@random.names','1970-04-09','M', 1);

-- On Duplicate nos ayuda a manejar el caso de que una llave que debe ser única (email) y se intente repetir
INSERT INTO clients (name, email, birthdate, gender, active)
VALUES ('Maria Dolores Gomez','Maria Dolores.95983222J@random.names','1971-06-06','F',0)
ON DUPLICATE KEY UPDATE active = VALUES(active) -- Values toma el el valor active del query insert



-- ▒█▀▀█ █▀▀█ █▀▀█ █░█ █▀▀ 
-- ▒█▀▀▄ █░░█ █░░█ █▀▄ ▀▀█ 
-- ▒█▄▄█ ▀▀▀▀ ▀▀▀▀ ▀░▀ ▀▀▀

-- Insert normal
INSERT INTO books (title, author_id, year)
VALUES ('El laberitno de la soledad', 6, 1950);

-- Insert con query anidado, se debe usar con ciudado por performance.
INSERT INTO books (title, author_id, year)
VALUES ('El mono gramático', (
  SELECT author_id from authors WHERE name = 'Octavio Paz'
), 1974);

