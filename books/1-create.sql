CREATE TABLE IF NOT EXISTS books (
  book_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  author_id INTEGER UNSIGNED NOT NULL ,
  title VARCHAR(100) NOT NULL,
  year INTEGER UNSIGNED NOT NULL,
  lamguage VARCHAR(2) NOT NULL DEFAULT 'es' COMMENT 'ISO 639-1 Language',
  cover_url VARCHAR(500),
  -- En elementos double, el primer dígito (6) indica las posiciones a reservar, y el segundo, de esas posiciones, cuántas seran para decimales. 
  price DOUBLE(6, 2) NOT NULL DEFAULT 10.0,
  -- Bandera True 1, False 0
  sellable TINYINT(1) DEFAULT 1,
  description TEXT
);

CREATE TABLE IF NOT EXISTS authors (
  author_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  nationality VARCHAR(3)
);

CREATE TABLE IF NOT EXISTS clients (
  client_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  -- Las `` permiten usar una pablra reservada (name) como nombre de la columna
  `name` VARCHAR(50) NOT NULL,
  email VARCHAR(100) NOT NULL UNIQUE,
  -- Permite un valor menor a epoc
  birthdate DATETIME,
  -- TIMESTAMP se basa en epoc (segundos desde 1-enero-1970)
  -- El default CURRENT_TIMESTAMP asigna el momento de la creación del resgistro a este campo
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  -- ON UPDATE asignara el valor al campo cuando se actualice el registro
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  -- ENUM nos permite dar opciones para los valores en este campo
  gender ENUM('M', 'F', 'ND') NOT NULL,
  -- Columna para borrado lógico
  active TINYINT(1) NOT NULL DEFAULT 1
);

CREATE TABLE IF NOT EXISTS operations (
  operation_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  book_id INTEGER UNSIGNED NOT NULL,
  client_id INTEGER UNSIGNED NOT NULL,
  `type` ENUM('prestado', 'devuelto', 'vendido'),
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  finished TINYINT(1) NOT NULL DEFAULT 0
);
