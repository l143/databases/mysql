-- Limit indica cuántas líneas debe traer.

SELECT name FROM clients LIMIT 1;

-- Existen algunas funciones propias de mysql como YEAR

SELECT YEAR(birthdate) FROM clients;

-- Cálcullo básico (no exacto), el 'as' noe permite renombrar la tabla en los resultados

SELECT name, YEAR(NOW()) - YEAR(birthdate) as edad FROM clients;


-- Like es una condicional de cercañia de textos
-- % es una especie de wildcard qyue indica que no importa el texto que esté ahí;

SELECT name FROM clients WHERE name LIKE '%Fer%';

-- Podemos añadir varias condiciones del where con and

SELECT * FROM clients WHERE client_id > 0 and client_id <=5;

-- BETWEEN nos ayuda a acortar el query

SELECT * FROM clients WHERE client_id BETWEEN 1 AND 5;
