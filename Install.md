# Mysql

## 🚪 Instalación

La forma más sencilla de iniciar un servidor de mysql es por medio de [docker](https://hub.docker.com/_/mysql). 

```bash
$ docker run --name localmysql -p 1000:3306 -e MYSQL_DATABASE=prueba -e MYSQL_USER=someUser -e MYSQL_PASSWORD=somePass -e MYSQL_ROOT_PASSWORD=admin  mysql:8.0
```

| Variable | valor |
|--|--|
| MYSQL_USER | someUser | 
| MYSQL_PASSWORD | somePass |
| MYSQL_DATABASE | prueba |
| MYSQL_ROOT_PASSWORD | admin | 

---

## 💻 Conexión por cliente de terminal

Para conectarnos por la termnal, debemos tener disponible el cliente de mysql.

```bash
$ sudo apt-get install mysql-client
```
 Una vez que tengamos el cliente podemos conectaros al servidor.

```bash
$ mysql -h 192.168.1.143 -P 1000 -u someUser -D prueba -p
```
| Bandera | Descripción |
| -- | -- |
| h | Indica el host o dirección del servidor |
| P | Indica el puerto para la conexión |
| u | Usuario de la conexión (debe tener permiso a la base si ésta se especifica) |
| D | Indica el nombre de la base a la que se conectará el cliente |
| p | Indica que le pasaremos la contraseña por el prompt |
